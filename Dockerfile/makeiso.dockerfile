FROM archlinux/base:latest

RUN : \
    # Install packages for makeiso
    && pacman -Syyu --noconfirm \
    && pacman -S --noconfirm \
        archiso \
        base \
        git \
        rsync \
    # Proxy
    && echo 'Defaults env_keep="http_proxy https_proxy"' >> /etc/sudoers \
    # Show installed packages
    && pacman -Qe \
    # Clear cache
    && rm -rf \
        /usr/share/doc/* \
        /usr/share/man/* \
        /var/cache/pacman/pkg/* \
        /var/lib/pacman/sync/*

CMD ["/usr/bin/bash"]
